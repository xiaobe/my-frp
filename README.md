## 基本配置

`conf/frps.ini` 为server配置
`conf/frpc.ini` 为client配置

### frps配置([查看全部配置](https://github.com/fatedier/frp/blob/master/conf/frps_full.ini))

除了`subdomain_host = frp.xn--9wy.top` 以外基本不用改变

注意服务器需要开启端口 `6000 7000 8899`

### frpc配置([查看全部配置](https://github.com/fatedier/frp/blob/master/conf/frpc_full.ini))

建立代理 
```
[http_proxy]
type = tcp
remote_port = 6000
plugin = socks5
```

建立本地网站
```
[web]
type = http
local_ip = docker.for.mac.localhost
local_port = 8080
subdomain = phl
# custom_domains = web01.yourdomain.com
```


## 使用

### frps使用

将项目文件放到服务器后，执行
```
docker-compose up -d frp-server
```

如果需要配置`nginx`可以参照`frp_nginx.conf`进行配置

### frpc 使用

配置好frps后，到客户端机器，执行
```
docker-compose up -d frp-client
```



